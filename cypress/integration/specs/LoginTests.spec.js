describe("Login Tests", () => {
  beforeEach(() => {
    Cypress.on("uncaught:exception", (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false;
    });

    // Go to site
    cy.visit("https://bioskoponline.com");

    // Click button Masuk for Login
    cy.contains("Masuk").click().should("contain.text", "Masuk");
  });

  it("Check user Login with valid data", () => {
    // Enter Email
    cy.get(".input-custom").type("sandirpl3e@gmail.com");

    // Enter Password
    cy.get('input[type="password"]').type("Katasandiku123");

    // Click button Masuk
    cy.get('form[class="mb-6"]').submit();
    cy.get('.toasted').should('contain.text', 'Masuk berhasil')

    // Delay Time
    cy.wait(8000);

    // Logout process
    cy.get(".bg-blue-4 > .cursor-pointer").click();
    cy.get(".mt-4").click()
    cy.get(".text-white > .flex > .mr-5").click()

  });


  it("Check user Login with invalid data", () => {
    // Enter Email
    cy.get(".input-custom").type("sandirpl3e@gmail.com");

    // Enter Password
    cy.get('input[type="password"]').type("1234567890");

    // Click button Masuk
    cy.get('form[class="mb-6"]').submit();

    // Verify message for password wrong
    cy.get(".p-2").should(
      "contain.text",
      "Email, nomor ponsel / kata sandi kamu salah."
    );
  });
});
